# Node JS API Example

This is a very simple example that highlights how to use the FrostSmart Public API. The main purpose is to get you started and to get a practical example on how to access
the data of your customers. The API in version 1 focuses exclusively on extracting metric data based on the installed hardware in the field. When we talk about the hardware, we refer
primarily to the gateways (aka units) and the IoT radios. Of course it also includes the sensors, which are attached to the units and radios and which actually report the metric 
data. With the API you are able to create a session, find out which units you can access, get a list of all metrics that the system is reporting and finally query the system for raw or 
aggregated data. If you want to find out more about the API and how to make the most out of it, please contact FrostSmart for the API documentation.

## Prerequisites
Before you get started, a few things need to be established.
1. Register with FrostSmart as a user. This will provide you a login and pwd. Usually you will get access to the data of your customer at that time as well.
2. Register with FrostSmart as a developer. You will receive an API KEY that will allow you to access the API.
3. Download an example and get started ... 

## Installation
Well, get the code and install the packages with:
```
npm install
```
## Configure your project
In order to run the first example, configure you login, pwd and api key. You find an object on top of index.js, where you can add this information. 

```
const Constants = {
    API_KEY: 'your api key granted by FrostSmart',
    USER: 'user login to access your sites',
    PWD: ' user pwd',
    REGION: 'ap-southeast-2'
};
```

## Run
Once the project is configured, just run the code and explore the output.
```
npm start
```

# Python API Example
This example accomplishes the exact same as the node js example. Please see above.

## Prerequisites
Same requisites as for nodejs. Please see above.

## Installation
You will need to install 2 packages in order to sign and perform API requests

```
pip install requests
pip install requests-aws4auth
```

## Configure your project
In order to run the first example, configure you login, pwd and api key. You find an object on top of index.js, where you can add this information. 

```
config = {'API_KEY': 'your api key granted by Loncel',
          'USER': 'user login to access your sites',
          'PWD': 'user pwd',
          'REGION': 'ap-southeast-2'
         }    
```

## Run
Once the project is configured, just run the code and explore the output.
```
python example.py
```