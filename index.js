"use strict";
/**
 * @file index.js
 * @description Example code to demonstrate the usage of the Loncel API
 *
 * @author Dirk Grobler
 * @copyright Copyright 2021 - FrostSmart Ltd. All Rights Reserved.
 */
let axios = require('axios');
let aws4 = require('aws4');

const {
    authenticate
} = require('./auth');
const Constants = require('./constants');

// const Constants = {
//     API_KEY: 'your api key granted by Loncel',
//     USER: 'user login to access your sites',
//     PWD: ' user pwd',
//     REGION: 'ap-southeast-2'
// };

async function queryMetrics(query, credentials) {
    const opts = aws4.sign({
        method: 'POST',
        host: 'api.frostsmart.com',
        path: '/v1/metrics/query',
        headers: {
            "x-api-key": Constants.API_KEY
        },
        service: 'execute-api',
        region: Constants.REGION,
        body: JSON.stringify(query)
    }, credentials);

    return axios({
        method: 'POST',
        url: 'https://' + opts.host + opts.path,
        headers: opts.headers,
        data: query
    });
}

/**
 * fetching all known metric definitions
 * They are important in order to retrieve metric data from the backend. 
 *
 * @param {*} params
 * @returns
 */
async function fetchMetrics(credentials) {

    // each request needs to be signed 
    // it also requires a valid api key
    const opts = aws4.sign({
        method: 'GET',
        host: 'api.frostsmart.com',
        path: '/v1/metrics/list',
        headers: {
            "x-api-key": Constants.API_KEY
        },
        region: Constants.REGION,
        service: 'execute-api'
    }, credentials);

    const result = await axios({
        url: 'https://' + opts.host + opts.path,
        headers: opts.headers
    });
    return result.data.metrics;
}

/**
 * fetching all units (gateways) that the user has permissions to view
 *
 * @param {*} params
 * @returns
 */
async function fetchUnits(credentials) {

    // each request needs to be signed 
    // it also requires a valid api key
    const opts = aws4.sign({
        method: 'GET',
        host: 'api.frostsmart.com',
        path: '/v1/units',
        headers: {
            "x-api-key": Constants.API_KEY
        },
        region: Constants.REGION,
        service: 'execute-api'
    }, credentials);

    const result = await axios({
        url: 'https://' + opts.host + opts.path,
        headers: opts.headers
    });
    return result.data.units;
}

/**
 * fetches the sensors for a given unit
 * @param {*} credentials 
 * @param {*} unit 
 */
async function fetchSensors(credentials, unit) {
    const opts = aws4.sign({
        method: 'GET',
        host: 'api.frostsmart.com',
        path: `/v1/sensors?unit=${unit}`,
        headers: {
            "x-api-key": Constants.API_KEY
        },
        region: Constants.REGION,
        service: 'execute-api'
    }, credentials);

    const result = await axios({
        url: 'https://' + opts.host + opts.path,
        headers: opts.headers
    });
    return result.data.sensors;
}

async function main() {
    try {
        const { credentials } = (await authenticate(Constants));        
        const metrics = await fetchMetrics(credentials);

        // iterate over all existing metrics
        console.log(' SYSTEM METRICS \n\n\n\n');
        metrics.forEach(metric => {
            console.log(JSON.stringify(metric));
        });

        // now iterate over all units
        console.log('\n\n\n\n AVAILABLE UNITS \n\n\n\n');
        const units = await fetchUnits(credentials);
        units.forEach(units => {
            console.log(JSON.stringify(units));
        });

        console.log('\n\n\n\n AVAILABLE SENSORS for unit 474 \n\n\n\n');
        const sensors = await fetchSensors(credentials, 474);
        sensors.forEach(sensor => {
            console.log(JSON.stringify(sensor));
        });

        // query the avg for water volume metric for unit 474 broken down by sensor (water meter)
        console.log('\n\n\n\nWATER VOLUME for Meters attached to unit 474, last week\n\n\n\n');
        const query = {
            "startRelative": {
                "value": "1",
                "unit": "weeks"
            },
            "metrics": [{
                "name": "loncel.water.volume",                
                "groupBy": ["sensor"], // break the result down by sensor, other options are 'unit', 'site' or 'area'
                "filter": {
                    "unit": "474"
                },
                "aggregator": { // if aggregators are omitted, raw data is returned
                    "name": "avg", // could be one of ('avg','min','max','first','last','sum', 'count')
                    "sampling": {
                        "value": 1,
                        "unit": "days"
                    }
                }
            }]
        };
        const result = await queryMetrics(query, credentials);
        console.log(JSON.stringify(result.data));
    } catch (e) {
        console.log(e);
    }
}

main();