"use strict";
/**
 * @file auth.js
 * @description functions to authenticate and establish a valid session. Authentication 
 * requires 2 steps. 
 * 1.) A user, who is registered with Loncel needs to authenticate
 * 2.) Temporary session credentials need to be obtained to sign subsequent API requests
 *
 * @author Dirk Grobler
 * @copyright Copyright 2019 - Lonceltech Ltd. All Rights Reserved.
 */
let axios = require('axios');
let AWS = require('aws-sdk');

/**
 * Fetching the session credentials with authentication data
 * that has been returned as a result of a successful authentication
 * This method is optional if you request the credentials right away with the login.
 * 
 * Credentials are usually valid for 60 minutes. While a user token is valid 24hrs. If the api usage is just to 
 * fetch a bit of data, then there is no need to use this method. However, if your session is longer than 60mins
 * you will require to fetch/refresh credentials
 *
 * @param {*} authData
 * @param {*} config
 * @returns
 */
function fetchSessionCredentials(authData, config) {
    AWS.config.update({
        region: config.REGION
    });

    let credentials = new AWS.CognitoIdentityCredentials({
        IdentityId: authData.identityId,
        IdentityPoolId: authData.identityPoolId,
        Logins: {
            'cognito-identity.amazonaws.com': authData.token
        },
        LoginId: authData.loginId
    });    

    return new Promise((resolve, reject) => {
        credentials.refresh((err) => {
            if (err) {
                reject(err);
            } else {
                resolve(credentials);
            }
        });
    });
}

/**
 * Authenticates the user of the api. It requires the user identifier and password.
 * Usually you also provide the region, which should be 'ap-southeast2'
 *
 * @returns
 */
async function authenticate(config) {        
    const opts = {
        method: 'POST',
        host: 'api.loncel.com', 
        path: '/v1/user/login',
        headers: {   
            "x-api-key": config.API_KEY         
        },
        data: {
            "user": config.USER,
            "password": config.PWD,
            "region": config.REGION,
            "includeCredentials": true
        }
    };

    const response = await axios({
        method: 'POST',
        url: 'https://' + opts.host + opts.path,
        data: opts.data,
        headers: opts.headers
    });
        
    //console.log(`status: ${response.status}`);    
    return response.data;
}

module.exports = {
    authenticate,
    fetchSessionCredentials
};